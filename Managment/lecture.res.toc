\babel@toc {russian}{}
\contentsline {section}{\numberline {1}Базовые определения}{3}%
\contentsline {section}{\numberline {2}Управленческие роли}{4}%
\contentsline {subsection}{\numberline {2.1}Управление}{5}%
\contentsline {subsubsection}{\numberline {2.1.1}Планирование}{5}%
\contentsline {subsubsection}{\numberline {2.1.2}Внутренняя среда}{6}%
\contentsline {subsubsection}{\numberline {2.1.3}Внешняя среда}{6}%
\contentsline {subsubsection}{\numberline {2.1.4}SWOT-анализ}{6}%
\contentsline {section}{\numberline {3}Стратегия фирмы}{7}%
\contentsline {subsection}{\numberline {3.1}Бостонская матрица}{7}%
\contentsline {subsection}{\numberline {3.2}Стратегия фирмы}{7}%
\contentsline {subsection}{\numberline {3.3}Корпоративные стратегии}{7}%
\contentsline {subsection}{\numberline {3.4}Универсальные стратегии (по Майклу Портеру)}{8}%
\contentsline {subsection}{\numberline {3.5}Особенности универсальных стратегий}{9}%
\contentsline {subsubsection}{\numberline {3.5.1}Продуктово-маркетинговые стратегии}{9}%
\contentsline {subsubsection}{\numberline {3.5.2}Инновационная стратегия}{9}%
\contentsline {subsubsection}{\numberline {3.5.3}Стратегия обновления продукта}{10}%
\contentsline {subsubsection}{\numberline {3.5.4}Стратегия постепенного совершенствования продукта}{10}%
\contentsline {subsection}{\numberline {3.6}Планирование}{10}%
\contentsline {subsubsection}{\numberline {3.6.1}Стратегическое планирование}{10}%
\contentsline {subsection}{\numberline {3.7}Бюджет}{10}%
\contentsline {subsection}{\numberline {3.8}Оперативное планирование}{11}%
\contentsline {subsection}{\numberline {3.9}Документы бюджетного плана}{11}%
\contentsline {section}{\numberline {4}Методика прогнозирования спроса}{11}%
\contentsline {subsection}{\numberline {4.1}Планирование продаж, закупок и запасов }{12}%
\contentsline {subsubsection}{\numberline {4.1.1}Подготовка данных для анализа}{12}%
\contentsline {subsubsection}{\numberline {4.1.2}Метод скользящего среднего}{12}%
\contentsline {subsubsection}{\numberline {4.1.3}Линейная аппроксимация методом наименьших квадратов}{12}%
\contentsline {subsubsection}{\numberline {4.1.4}Метод экспоненциального сглаживания с учетом тренда}{13}%
\contentsline {subsubsection}{\numberline {4.1.5}Метод Кростона (Croston)}{13}%
\contentsline {subsubsection}{\numberline {4.1.6}Производственный план}{13}%
\contentsline {section}{\numberline {5}Организация}{14}%
\contentsline {subsection}{\numberline {5.1}Делегирование полномочий}{14}%
\contentsline {subsection}{\numberline {5.2}Норма управляемости}{14}%
\contentsline {subsection}{\numberline {5.3}Линейная организационная структура}{15}%
\contentsline {subsection}{\numberline {5.4}Линейно-штабная организационная структура}{15}%
\contentsline {subsection}{\numberline {5.5}Функциональная организационная структура}{16}%
\contentsline {subsection}{\numberline {5.6}Линейно-функциональная организационная структура}{16}%
\contentsline {subsection}{\numberline {5.7}Дивизиональная организационная структура}{16}%
\contentsline {subsection}{\numberline {5.8}Матричная организационная структура}{17}%
\contentsline {section}{\numberline {6}Бизнес-процессы}{17}%
\contentsline {subsection}{\numberline {6.1}Методы управления}{17}%
\contentsline {subsection}{\numberline {6.2}Бизнес-процессы}{17}%
\contentsline {subsection}{\numberline {6.3}Нотация eEPC}{18}%
\contentsline {subsection}{\numberline {6.4}Управление проектами}{18}%
\contentsline {section}{\numberline {7}Мотивация}{18}%
\contentsline {subsection}{\numberline {7.1}Мотивация через потребности}{18}%
\contentsline {subsection}{\numberline {7.2}Пирамида Маслоу}{18}%
\contentsline {subsection}{\numberline {7.3}Методы удовлетворения потребностей высших уровней}{19}%
\contentsline {subsection}{\numberline {7.4}Типология Майерс-Бриггс}{19}%
\contentsline {section}{\numberline {8}Контроль}{19}%
\contentsline {subsection}{\numberline {8.1}Цели и показатели}{20}%
\contentsline {subsection}{\numberline {8.2}НИОКР}{20}%
\contentsline {subsection}{\numberline {8.3}Контроль}{20}%
\contentsline {subsection}{\numberline {8.4}Управленческий учет (контроллинг)}{20}%
\contentsline {subsection}{\numberline {8.5}Функции контроллинга}{21}%
\contentsline {subsection}{\numberline {8.6}Рекомендации по проведению эффективного контроля}{21}%
\contentsline {section}{\numberline {9}Типология Майрес-Бриггс}{22}%
\contentsline {section}{\numberline {10}Регулирование}{22}%
\contentsline {subsection}{\numberline {10.1}Отклонения}{22}%
\contentsline {subsection}{\numberline {10.2}Причины отклонений (примеры)}{22}%
\contentsline {subsection}{\numberline {10.3}Разработка и принятие управленческих решений}{22}%
\contentsline {subsection}{\numberline {10.4}Логика выбора вариантов в модели Врума}{23}%
\contentsline {section}{\numberline {11}Оценка эффективности решений}{23}%
\contentsline {subsection}{\numberline {11.1}Инвестиционные решения}{23}%
\contentsline {subsection}{\numberline {11.2}Процесс оценки проектов}{23}%
\contentsline {section}{\numberline {12}Показатели качества}{23}%
\contentsline {subsection}{\numberline {12.1}Время окупаемости}{23}%
\contentsline {subsection}{\numberline {12.2}Доходность проекта}{24}%
\contentsline {subsection}{\numberline {12.3}Почему деньги дешевеют?}{24}%
\contentsline {subsection}{\numberline {12.4}Дисконтирование}{24}%
\contentsline {subsection}{\numberline {12.5}Приведенная стоимость}{24}%
\contentsline {subsection}{\numberline {12.6}Дисконтируемый период окупаемости}{24}%
\contentsline {subsection}{\numberline {12.7}Чистая приведенная стоимость}{25}%
\contentsline {subsection}{\numberline {12.8}Индекс доходности}{25}%
\contentsline {subsection}{\numberline {12.9}Внутренняя норма доходности}{25}%
\contentsline {subsection}{\numberline {12.10}Цена окупаемости}{25}%
\contentsline {subsection}{\numberline {12.11}Взаимоисключающие альтернативы}{25}%
