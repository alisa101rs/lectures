from matplotlib import pyplot as plt

plt.figure()
plt.plot([0,0.5, 1, 2, 3, 4, 6, 10 ], [[1 - 1 / (1 + 0.2*m**2)] for m in [0,0.5, 1, 2, 3, 4, 6, 10 ]])
plt.xlabel("M")
plt.ylabel(r"$ \eta_t $")
plt.savefig("draw/31.png")

#ДЗ
k = 1.4
R = 8314/29.0


i = 4
H = 6 + 0.1*(i-1)
alpha = 2 + 0.05*(i-1)
M_h = 0.8 + 0.005*(i-1)
F_2 = 0.5
mu = 29e-3

#g_C = 80 + 0.5 * (i-1)
F_2F_1 = 1.5 + 0.03*(i-1)
sigma_c = 0.95 + 0.001*(i-1)

p0 = 1e5
p_h = p0*(1 - H*1000/44300)**5.256
rho0 = 1
rho_h = rho0*(1 - H*1000/44300)**4.256

T_h = p_h/rho_h/R

a_zv = (k*R*T_h)**0.5
w_h = M_h*a_zv

T0_h = (1 + (k-1)/2*M_h**2)*T_h

a_kr = (2*k/(k+1)*R*T0_h)**0.5

l_h = w_h/a_kr


g_c = 0.8 + 0.5/100*(i-1)
g_h = 1 - g_c
g_o = 0.23
g_N = 0.77
mu_c = 12e-3
mu_h = 1e-3
mu_o = 16e-3
mu_n = 14e-3

def Km0():
    g_h = 1 - g_c
    g_o = 0.23
    g_N = 0.77
    mu_c = 12e-3
    mu_h = 1e-3
    mu_o = 16e-3
    mu_n = 14e-3
    C = g_c/mu_c
    H = g_h/mu_h
    N = g_N/mu_n
    O = g_o/mu_o
    K_m0 = - (-H -4*C)/(2*O)
    return K_m0
K_m0 = Km0()

K_m = alpha*K_m0

w_2 = [20*i for i in range(1, 8)]

xxH = [0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6,0.8,1.0]
yyH = [0.36, 0.49, 0.65, 0.76, 0.83, 0.89, 0.93, 0.98, 1.0]














