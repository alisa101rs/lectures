import pathlib
from pathlib import Path
import subprocess


files = Path(".").glob("*.svg")

for file in files:
    pdf = file.stem + ".pdf"
    subprocess.run([
        'inkscape',
        '--export-area-drawing',
        '--export-dpi', '300',
        '--export-pdf', f'{pdf}',
        '--export-latex', f'{file}'])







